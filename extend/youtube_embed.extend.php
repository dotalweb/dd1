<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// 밑의 return; 코드의 주석을 해제하면 사용 안함
//return;

add_event('tail_sub', 'youtube_content_embed_fn_script', 0, 0);

function youtube_content_embed_fn_script(){
?>
<script>
jQuery(function($){

var youtube_content_embed = {
    createYoutubeEmbed : function(str, key) {

        var $html = jQuery(str),
            $iframe = jQuery("<iframe>", { "src":"https://www.youtube.com/embed/"+key });
        
        if( ! $html.find($iframe).length ){
            return str+'<div><iframe width="100%" height="540" src="https://www.youtube.com/embed/' + key + '" frameborder="0" allowfullscreen></iframe></div>';
        }
        return str;
    },

    transformYoutubeLinks : function(text, selector) {
        if (!text) return text;
        var self = this;

        var linkreg = /(?:)<a([^>]+)>(.+?)<\/a>/g,
            fullreg = /(https?:\/\/)?(www\.)?(youtube\.com\/watch\?v=|youtu\.be\/)([^& \n<]+)(?:[^ \n<]+)?/g,
            regex = /(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/watch\?v=|youtu\.be\/)([^& \n<]+)(?:[^ \n<]+)?/g,
            resultHtml = String(text),
            match = text.match(fullreg),
            el_html = jQuery("<div/>").html(text),
            keys = [];

        if (match && match.length > 0) {

            var matchlinks = text.match(linkreg);

            if (matchlinks && matchlinks.length > 0) {
                for (var i=0; i < matchlinks.length; i++) {
                    var matchParts = matchlinks[i].split(regex),
                        match_key = matchParts[1] ? matchParts[1].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '') : '';

                    if( match_key && keys.indexOf(match_key) === -1 ){
                        keys.push( match_key );
                        resultHtml = resultHtml.replace(matchlinks[i], self.createYoutubeEmbed(matchlinks[i], match_key) );
                    }
                }
            }

            for (var i=0; i < match.length; i++) {
                var matchParts = match[i].split(regex),
					match_key = matchParts[1] ? matchParts[1].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '') : '';

                if( match_key && ! $(el_html).find("a:contains('"+match[i]+"')").length ){
                    if( keys.indexOf(match_key) === -1 ){
                        keys.push( match_key );
                        resultHtml = resultHtml.replace(match[i], self.createYoutubeEmbed(match[i], match_key) );
                    }
                }
            }
        }

        return resultHtml;
    }
}

function youtube_content_replace(selector, i){

    var cm = (typeof selector !== 'undefined') ? selector : "";

    if( cm && jQuery(cm).length ){
        jQuery(cm).each(function(index, item){
            
            var ori_html = jQuery(item).html();
            jQuery(item).html( youtube_content_embed.transformYoutubeLinks(ori_html) );
        });
    }
}

youtube_content_replace("#bo_v_con");   // 글 본문
youtube_content_replace(".cmt_contents");   // 코멘트들

});
</script>
<?php
}   //end php function
?>