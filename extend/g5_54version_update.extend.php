<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

add_event('memo_list', 'g54_user_memo_insert', 10, 3);
add_event('password_is_wrong', 'g54_check_bbs_password', 10, 3);
add_event('bbs_good_after', 'g5recomment', 10, 3);
add_replace('invalid_password', 'g54_return_invalid_password', 10, 3);

function g54_return_invalid_password($bool, $type, $wr){
    if($type === 'write' && $bool === false && $wr['wr_password'] && isset($_POST['wr_password'])) {
        if(G5_STRING_ENCRYPT_FUNCTION === 'create_hash' && (strlen($wr['wr_password']) === G5_MYSQL_PASSWORD_LENGTH || strlen($wr['wr_password']) === 16)) {
            if( sql_password($_POST['wr_password']) === $wr['wr_password'] ){
                $bool = true;
            }
        }
    }

    return $bool;
}

function g5recomment($bo_table, $wr_id, $good){

	global $g5, $board;
		
	if($good == "good"){
		
		$row = sql_fetch (" select wr_good, wr_nogood from {$g5['write_prefix']}{$bo_table} where wr_id = '{$wr_id}' ");
		
		if ( $board['bo_recomment_cnt'] > '0' && $row['wr_good'] == $board['bo_recomment_cnt']){

			$sql2 = " select * from {$g5['write_prefix']}{$bo_table} where wr_id = '{$wr_id}'";
			$result2 = sql_fetch_array(sql_query($sql2));
			
			$move_bo_table = "recomment";
			$move_write_table = $g5['write_prefix'] . $move_bo_table;

			$src_dir = G5_DATA_PATH.'/file/'.$bo_table; // 원본 디렉토리
			$dst_dir = G5_DATA_PATH.'/file/'.$move_bo_table; // 복사본 디렉토리
			
			$save = array();
			$count_write = 0;
			$count_comment = 0;

			$next_wr_num = get_next_num($move_write_table);

			$sql2 = " select * from {$g5['write_prefix']}{$bo_table} where wr_num = '{$result2['wr_num']}' order by wr_parent, wr_is_comment, wr_comment desc, wr_id ";
			$result2 = sql_query($sql2);
			while ($row2 = sql_fetch_array($result2))
			{
				if($row2['ca_name'])
					$row2['wr_subject'] = "[".$row2['ca_name']."] ".$row2['wr_subject'];	
				
				$sql = " insert into $move_write_table
							set wr_num = '$next_wr_num',
								 wr_reply = '{$row2['wr_reply']}',
								 wr_is_comment = '{$row2['wr_is_comment']}',
								 wr_comment = '{$row2['wr_comment']}',
								 wr_comment_reply = '{$row2['wr_comment_reply']}',
								 ca_name = '".addslashes($row2['ca_name'])."',
								 wr_option = '{$row2['wr_option']}',
								 wr_subject = '".addslashes($row2['wr_subject'])."',
								 wr_content = '".addslashes($row2['wr_content'])."',
								 wr_link1 = '".addslashes($row2['wr_link1'])."',
								 wr_link2 = '".addslashes($row2['wr_link2'])."',
								 wr_link1_hit = '{$row2['wr_link1_hit']}',
								 wr_link2_hit = '{$row2['wr_link2_hit']}',
								 wr_hit = '{$row2['wr_hit']}',
								 wr_good = '{$row2['wr_good']}',
								 wr_nogood = '{$row2['wr_nogood']}',
								 mb_id = '{$row2['mb_id']}',
								 wr_password = '{$row2['wr_password']}',
								 wr_name = '".addslashes($row2['wr_name'])."',
								 wr_email = '".addslashes($row2['wr_email'])."',
								 wr_homepage = '".addslashes($row2['wr_homepage'])."',
								 wr_datetime = '{$row2['wr_datetime']}',
								 wr_file = '{$row2['wr_file']}',
								 wr_last = '{$row2['wr_last']}',
								 wr_ip = '{$row2['wr_ip']}',
								 wr_1 = '".addslashes($row2['wr_1'])."',
								 wr_2 = '".addslashes($row2['wr_2'])."',
								 wr_3 = '".addslashes($row2['wr_3'])."',
								 wr_4 = '".addslashes($row2['wr_4'])."',
								 wr_5 = '".addslashes($row2['wr_5'])."',
								 wr_6 = '".addslashes($row2['wr_6'])."',
								 wr_7 = '".addslashes($row2['wr_7'])."',
								 wr_8 = '".addslashes($row2['wr_8'])."',
								 wr_9 = '".addslashes($row2['wr_9'])."',
								 wr_10 = '".addslashes($row2['wr_10'])."', 
								 wr_singo = '{$row2['wr_singo']}' ";
				sql_query($sql);
				$insert_id = sql_insert_id();

				// 코멘트가 아니라면
				if (!$row2['wr_is_comment'])
				{
					$save_parent = $insert_id;
					$sql3 = " select * from {$g5['board_file_table']} where bo_table = '$bo_table' and wr_id = '{$row2['wr_id']}' order by bf_no ";
					$result3 = sql_query($sql3);
					for ($k=0; $row3 = sql_fetch_array($result3); $k++)
					{
						if ($row3['bf_file'])
						{
							$copy_file_name = ($bo_table !== $move_bo_table) ? $row3['bf_file'] : $row2['wr_id'].'_copy_'.$insert_id.'_'.$row3['bf_file'];
							$is_exist_file = is_file($src_dir.'/'.$row3['bf_file']) && file_exists($src_dir.'/'.$row3['bf_file']);
							if( $is_exist_file ){
								@copy($src_dir.'/'.$row3['bf_file'], $dst_dir.'/'.$copy_file_name);
								@chmod($dst_dir.'/'.$row3['bf_file'], G5_FILE_PERMISSION);
							}
						}

						$sql = " insert into {$g5['board_file_table']}
									set bo_table = '$move_bo_table',
										 wr_id = '$insert_id',
										 bf_no = '{$row3['bf_no']}',
										 bf_source = '".addslashes($row3['bf_source'])."',
										 bf_file = '$copy_file_name',
										 bf_download = '{$row3['bf_download']}',
										 bf_content = '".addslashes($row3['bf_content'])."',
										 bf_fileurl = '".addslashes($row3['bf_fileurl'])."',
										 bf_thumburl = '".addslashes($row3['bf_thumburl'])."',
										 bf_storage = '".addslashes($row3['bf_storage'])."',
										 bf_filesize = '{$row3['bf_filesize']}',
										 bf_width = '{$row3['bf_width']}',
										 bf_height = '{$row3['bf_height']}',
										 bf_type = '{$row3['bf_type']}',
										 bf_datetime = '{$row3['bf_datetime']}' ";
						sql_query($sql);

					}
					$count_write++;

					sql_query(" update {$g5['scrap_table']} set bo_table = '$move_bo_table', wr_id = '$save_parent' where bo_table = '$bo_table' and wr_id = '{$row2['wr_id']}' ");
					sql_query(" update {$g5['board_new_table']} set bo_table = '$move_bo_table', wr_id = '$save_parent', wr_parent = '$save_parent' where bo_table = '$bo_table' and wr_id = '{$row2['wr_id']}' ");
					sql_query(" update {$g5['board_good_table']} set bo_table = '$move_bo_table', wr_id = '$save_parent' where bo_table = '$bo_table' and wr_id = '{$row2['wr_id']}' ");		
				} else {
					$count_comment++;
					sql_query(" update {$g5['board_new_table']} set bo_table = '$move_bo_table', wr_id = '$insert_id', wr_parent = '$save_parent' where bo_table = '$bo_table' and wr_id = '{$row2['wr_id']}' ");				
				}

				sql_query(" update $move_write_table set wr_parent = '$save_parent' where wr_id = '$insert_id' ");

				$cnt++;
			}

			sql_query(" update {$g5['board_table']} set bo_count_write = bo_count_write + '$count_write' where bo_table = '$move_bo_table' ");
			sql_query(" update {$g5['board_table']} set bo_count_comment = bo_count_comment + '$count_comment' where bo_table = '$move_bo_table' ");
			sql_query(" update {$g5['board_table']} set bo_count_write = bo_count_write - '$count_write' where bo_table = '$bo_table' ");
			sql_query(" update {$g5['board_table']} set bo_count_comment = bo_count_comment - '$count_comment' where bo_table = '$bo_table' ");			
		}			
	}
}


function g54_check_bbs_password($type, $wr, $qstr=''){
    if($type === 'bbs' && (isset($wr['wr_password']) && $wr['wr_password']) && isset($_POST['wr_password'])) {

        global $bo_table, $w;

        if(G5_STRING_ENCRYPT_FUNCTION === 'create_hash' && (strlen($wr['wr_password']) === G5_MYSQL_PASSWORD_LENGTH || strlen($wr['wr_password']) === 16)) {
            if( sql_password($_POST['wr_password']) === $wr['wr_password'] ){
                if ($w == 's') {
                    $ss_name = 'ss_secret_'.$bo_table.'_'.$wr['wr_num'];
                    set_session($ss_name, TRUE);
                } else if ($w == 'sc'){
                    $ss_name = 'ss_secret_comment_'.$bo_table.'_'.$wr['wr_id'];
                    set_session($ss_name, TRUE);
                }
                goto_url(short_url_clean(G5_HTTP_BBS_URL.'/board.php?'.$qstr));
            }
        }
    }
}

function g54_user_memo_insert($kind, $unkind, $page=1){
    global $g5, $is_member, $member;

    if( ! $is_member || $kind !== 'send' ) return;

    $sql = " select count(*) as cnt from {$g5['memo_table']} where me_send_mb_id = '{$member['mb_id']}' and me_type = 'recv' and me_send_ip = '' ";
    $row = sql_fetch($sql);

    if ( !$row['cnt'] ) return;

    $sql = " select count(*) as cnt from {$g5['memo_table']} where me_send_mb_id = '{$member['mb_id']}' and me_type = 'send' ";
    $row2 = sql_fetch($sql);

    if( $row['cnt'] && ! $row2['cnt'] ){
        $sql = " select * from {$g5['memo_table']} where me_send_mb_id = '{$member['mb_id']}' and me_type = 'recv' ";
        $result = sql_query($sql);

        while ($row = sql_fetch_array($result))
        {
            $sql = " insert into {$g5['memo_table']} ( me_recv_mb_id, me_send_mb_id, me_send_datetime, me_read_datetime, me_memo, me_send_id, me_type ) values ( '".addslashes($row['me_recv_mb_id'])."', '".addslashes($row['me_send_mb_id'])."', '".addslashes($row['me_send_datetime'])."', '".addslashes($row['me_read_datetime'])."', '".addslashes($row['me_memo'])."', '".$row['me_id']."', 'send' ) ";

            sql_query($sql);
        }

        $sql = " update {$g5['memo_table']} set me_send_ip = '{$_SERVER['REMOTE_ADDR']}' where me_send_mb_id = '{$member['mb_id']}' and me_type = 'recv' and me_send_ip = '' ";

        sql_query($sql);
    }

}
?>