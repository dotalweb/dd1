<?php
include_once("_common.php");

if (!$is_member)
    alert_close("회원만 이용하실 수 있습니다.");

if ($write['mb_id'] == $config['cf_admin'])
    alert_close("최고관리자의 글은 신고하실 수 없습니다.");

if ($write['mb_id'] == "admin" || $write['mb_id'] == "root" )
    alert_close("운영자의 글은 신고하실 수 없습니다.");

if ($write['mb_id'] == $member['mb_id'])
    alert_close("본인의 글은 신고할 수 없습니다.");


$sql = "select * from singo_log where bo_table = '$bo_table' and wr_id = '$wr_id' and (mb_id = '$member[mb_id]' or si_ip = '$_SERVER[REMOTE_ADDR]')";
$row = sql_fetch($sql);
if ($row && !$is_admin)
    alert_close("이미 신고하셨습니다.");

set_session("ss_singo_token", $token = uniqid(time()));

$meta = "<meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0\">";
ob_start();
include_once(G5_PATH."/head.sub.php");
$head = ob_get_clean();
$head = str_replace("<head>", "<head>\n{$meta}", $head);
echo $head;
?>
<link rel="stylesheet" href="<?php echo $board_skin_path?>/style.css" type="text/css">

<style type="text/css">
.title { font-size:15px;font-weight:bold; background-color:#f1f3f6; padding:10px; }
.btn { background-color:#efefef; cursor:pointer; font-size:1em;}
.singo_box {display:flex;align-items:center;width:100%;margin:15px 0px;}
.singo_box .box_subject {width:15%;text-align:center}
.singo_box .box_content {width:85%;}
.singo_box .box_content select {height:3em;line-height:3em;}
.singo_box .box_content textarea {width:100%}
</style>

<div class="title">게시물 신고하기</div>
<form method="post" action="btn.singo.update.php" style="margin:10px;">
<input type="hidden" name="bo_table" value="<?php echo $bo_table?>"/>
<input type="hidden" name="wr_id" value="<?php echo $wr_id?>"/>
<input type="hidden" name="parent_id" value="<?php echo $parent_id?>"/>
<input type="hidden" name="token" value="<?php echo $token?>"/>
<div class="singo_box">
	<div class="box_subject">신고대상</div>
	<div class="box_content">
		<?php 
			$sql = "select * from g5_write_{$bo_table} where wr_id = '{$wr_id}' ";
			$row = sql_fetch($sql);
			$mb = get_member($row['mb_id']);
			echo $mb['mb_nick'];
		?>
		<input type="hidden" name="si_mb_id" value="<?php echo $row['mb_id']?>"/>
    </div>
</div>
<div class="singo_box">
	<div class="box_subject">신고분류</div>
	<div class="box_content">
        <select name="category" itemname="분류" required>
			<option value="">선택하십시오.</option>
			<option value="무의미한 도배글">무의미한 도배글</option>
			<option value="게시판 성격과 맞지 않음">게시판 성격과 맞지 않음</option>			
			<option value="광고,홍보,방문유도 게시글">광고,홍보,방문유도 게시글</option>
			<option value="성희롱,욕설,비방,반사회적 게시글">성희롱,욕설,비방,반사회적 게시글</option>
			<option value="저작권법 위반 게시글">저작권법 위반 게시글</option>
        </select>
    </div>
</div>
<div class="singo_box">
	<div class="box_subject">신고내용</div>
	<div class="box_content">
		<textarea name="content" itemname="신고내용" rows="5" class="required" placeholder="신고 사유를 입력하세요"></textarea>
	</div>
</div>

<p align="center">
    <input type="submit" value="신고하기" class="btn">
    <input type="button" value="창닫기" onclick="self.close()" class="btn">
</p>

</form>
<?php include_once(G5_PATH."/tail.sub.php");?>

