<?php
include_once("_common.php");

if (!strstr($_SERVER['HTTP_REFERER'], "/proc/btn.singo.php"))
    alert_close("올바른 방법으로 이용해주세요.");

if (!$token or get_session("ss_singo_token") != $token) 
    alert_close("토큰 에러로 신고 불가합니다.");

unset($_SESSION["ss_singo_token"]);

if (!$is_member)
    alert_close("회원만 이용하실 수 있습니다.");

if ($write['mb_id'] == $config['cf_admin'])
    alert_close("최고관리자의 글은 신고하실 수 없습니다.");

if ($write['mb_id'] == $member['mb_id'])
    alert_close("본인의 글은 신고할 수 없습니다.");

$sql = "select * from singo_log where bo_table = '{$bo_table}' and wr_id = '{$wr_id}' and (mb_id = '{$member['mb_id']}' or si_ip = '{$_SERVER['REMOTE_ADDR']}')";
$row = sql_fetch($sql);
if ($row && !$is_admin)
    alert_close("이미 신고하셨습니다.");

//쪽지받을 아이디
$me_recv_mb_id = "admin,hangru";
$recv_list = explode(',', trim($me_recv_mb_id));
$member_list = array('id'=>array(), 'nick'=>array());

$comment = "";
if ($wr_id != $parent_id)
        $comment = "#c_{$wr_id}";

$url = G5_BBS_URL."/board.php?bo_table=".$bo_table."&wr_id=".$parent_id.$comment;

$me_memo = "게시물 신고가 접수되었습니다.\n
    분류 : {$category}
    내용 : {$content}

    주소 : {$url}";


for ($i=0; $i<count($recv_list); $i++) {
    $row = sql_fetch(" select mb_id, mb_nick, mb_open, mb_leave_date, mb_intercept_date from {$g5['member_table']} where mb_id = '{$recv_list[$i]}' ");
    if ($row) {
        $member_list['id'][]   = $row['mb_id'];
        $member_list['nick'][] = $row['mb_nick'];
    }
}

for ($i=0; $i<count($member_list['id']); $i++) {
    $tmp_row = sql_fetch(" select max(me_id) as max_me_id from {$g5['memo_table']} ");
    $me_id = $tmp_row['max_me_id'] + 1;

    $recv_mb_id   = $member_list['id'][$i];
    $recv_mb_nick = get_text($member_list['nick'][$i]);

    // 받는 회원 쪽지 INSERT
    $sql = " insert into {$g5['memo_table']} ( me_recv_mb_id, me_send_mb_id, me_send_datetime, me_memo, me_read_datetime, me_type, me_send_ip ) values ( '$recv_mb_id', '{$member['mb_id']}', '".G5_TIME_YMDHIS."', '$me_memo', '0000-00-00 00:00:00' , 'recv', '{$_SERVER['REMOTE_ADDR']}' ) ";

    sql_query($sql);

    if( $me_id = sql_insert_id() ){

        // 보내는 회원 쪽지 INSERT
        $sql = " insert into {$g5['memo_table']} ( me_recv_mb_id, me_send_mb_id, me_send_datetime, me_memo, me_read_datetime, me_send_id, me_type , me_send_ip ) values ( '$recv_mb_id', '{$member['mb_id']}', '".G5_TIME_YMDHIS."', '$me_memo', '0000-00-00 00:00:00', '$me_id', 'send', '{$_SERVER['REMOTE_ADDR']}' ) ";
        sql_query($sql);
		
		$member_list['me_id'][$i] = $me_id;
    }

    // 실시간 쪽지 알림 기능
    $sql = " update {$g5['member_table']} set mb_memo_call = '{$member['mb_id']}', mb_memo_cnt = '".get_memo_not_read($recv_mb_id)."' where mb_id = '$recv_mb_id' ";
    sql_query($sql);
}

$sql = "insert into singo_log set bo_table = '$bo_table' ";
$sql.= ", wr_id = '$wr_id'";
$sql.= ", mb_id = '{$member['mb_id']}'";
$sql.= ", si_type = '$category'";
$sql.= ", si_memo = '$content'";
$sql.= ", si_datetime = '".G5_TIME_YMDHIS."'";
$sql.= ", si_ip = '{$_SERVER['REMOTE_ADDR']}'";
$sql.= ", si_mb_id = '$si_mb_id'";
sql_query($sql);

sql_query("update $write_table set wr_singo = wr_singo + 1 where wr_id = '$wr_id'");
$write['wr_singo']++;

if ($write['wr_singo'] && $write['wr_singo'] >= '10'){
    $html = $secret = $mail = '';
    if (strstr($write['wr_option'], 'html1')) $html = 'html1';
    if (strstr($write['wr_option'], 'html2')) $html = 'html2';
    if (strstr($write['wr_option'], 'mail')) $mail = 'mail';
    $secret = 'secret';
    sql_query("update $write_table set wr_option = '$html,$secret,$mail' where wr_id = '$wr_id'");
}

alert_close("신고 하였습니다.\\n\\n관심에 감사드립니다.");
	