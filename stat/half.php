<?php
include_once("./_common.php");

if (!$_SESSION['ss_mb_id']) {
    header("location: ../index.php");
}

$g4[title] = "통계";
include_once("./_head.php");

@include ($g4['bbs_path']."/board_list.php");

if($_GET['y'] != '' && intval($_GET['y']) < intval(date("Y"))) {
    $ym = $_GET['y'];
    if($_GET['m'] != '' && ($_GET['m'] == '01' || $_GET['m'] == '07'))
        $ym .= $_GET['m'];
    else
        $ym .= "07";
} else {
    if(intval(date("m")) >= 7) {
        $ym = date("Y")."01";
    } else if(intval(date("Y")) > 2015) {
        $ym = (intval(date("Y")) - 1)."07";
    }
}
?>
<style>
table, tr, th, td {font-size:9pt;}
hr {width:750px; height:1px; background:#b2b2b2; margin-bottom:10px; border:0;}
hr.hr2 {background:#848484; margin-bottom:20px;}
span.board_title {display:block; font-weight:bold; font-size:12pt; width:750px; background:#d2d2d2;}
</style>
<div style="height:20px;"></div>
<div><a href="/stat/">월간 통계__<?=$ym ?></a></div>
<?php
@include "stat_half_".$ym.".html";

include_once("./_tail.php");
?>