<?
include_once("./_common.php");

//외부 접근시 차단
if($_SERVER['REMOTE_ADDR']!="180.68.207.239") {
    exit;
}

$ym = date("Ym",strtotime("-1 month"));
?>
<style>
table, tr, th, td {font-size:9pt;}
hr {width:750px; height:1px; background:#b2b2b2; margin-bottom:10px; border:0;}
hr.hr2 {background:#848484; margin-bottom:20px;}
span.stat_title {display:block; font-weight:bold; font-size:14pt; width:750px; background:#d2d2d2;}
span.board_title {display:block; font-weight:bold; font-size:12pt; width:750px; background:#e4e4e4;}
</style>
<div style="height:40px;"></div>
<span class="stat_title"><?=substr($ym, 0, 4)."년 ".substr($ym, 4, 2)."월"?></span><br/>
<div style="height:20px;"></div>
<?
$bo_arr = array("free", "bizarre", "masturbation", "curious");

$sql = "delete from hit_writer";
sql_query($sql);
for($idx = 0; $idx < count($bo_arr); $idx++) {
    $sql = "select bo_subject from $g4[board_table] where bo_table = '$bo_arr[$idx]'";
    $bo_re = sql_fetch($sql);

    $sql = "select wr_id, wr_subject, wr_is_comment, wr_comment, wr_name, wr_hit, mb_id
    from ".$g4['write_prefix'].$bo_arr[$idx]."
    where date_format(wr_datetime, '%Y%m%d') between date_format(date_add(now(), interval -1 month), '%Y%m01') and date_format(date_add(now(), interval -1 month), '%Y%m31')";

    $result = sql_query($sql);

    $hit = array();
    $cmt = array();
    $write = array();
    $reply = array();

    while($row = sql_fetch_array($result)) {
        if($row['wr_is_comment']==0) {
            $hit[$row['wr_hit']] = $row;
            $cmt[$row['wr_comment']] = $row;

            if(isset($write[$row['mb_id']])) $write[$row['mb_id']][0] += 1;
            else {
                $write[$row['mb_id']][0] = 1;
                $write[$row['mb_id']][1] = $row;
            }
        } else {
            if(isset($reply[$row['mb_id']])) $reply[$row['mb_id']][0] += 1;
            else {
                $reply[$row['mb_id']][0] = 1;
                $reply[$row['mb_id']][1] = $row;
            }
        }
    }
?>
<span class="board_title"><?=$bo_re['bo_subject']?></span><br/>
<table border="0" cellpadding="2" cellspacing="0">
<colgroup>
    <col style="width:200px;" />
    <col style="width:400px;" />
    <col style="width:100px;" />
</colgroup>
<tr>
    <th>글쓴이</th><th>제목</th><th>힛수</th>
</tr>
    <?
    krsort($hit);

    $i = 0;
    foreach($hit as $key => $value) {
        if($i++ >= 10) break;
        if($i <= 3) {
            $h_sql = "insert into hit_writer (bo_table, gubun, odr, mb_id) values ('".$bo_arr[$idx]."','hit',".($i).",'".$value['mb_id']."')";
            $result = sql_query($h_sql);
        }
        echo "<tr><td>".$value['wr_name']."</td><td><a href='".$g4['bbs_path']."/board.php?bo_table=".$bo_arr[$idx]."&wr_id=".$value['wr_id']."' target='_blank'>".$value['wr_subject']."</a></td><td>".$value['wr_hit']."</td></tr>";
    }
    ?>
</table>
<hr/>
<table border="0" cellpadding="2" cellspacing="0">
<colgroup>
    <col style="width:200px;" />
    <col style="width:400px;" />
    <col style="width:100px;" />
</colgroup>
<tr>
    <th>글쓴이</th><th>제목</th><th>댓글수</th>
</tr>
    <?
    krsort($cmt);

    $i = 0;
    foreach($cmt as $key => $value) {
        if($i++ >= 10) break;
        echo "<tr><td>".$value['wr_name']."</td><td><a href='".$g4['bbs_path']."/board.php?bo_table=".$bo_arr[$idx]."&wr_id=".$value['wr_id']."' target='_blank'>".$value['wr_subject']."</a></td><td>".$value['wr_comment']."</td></tr>";
    }
    ?>
</table>
<hr/>
<table border="0" cellpadding="0" cellspacing="0">
<tr>
    <td>
        <table border="0" cellpadding="2" cellspacing="0">
        <colgroup>
            <col style="width:200px;" />
            <col style="width:100px;" />
        </colgroup>
        <tr>
            <th>글쓴이</th><th>글수</th>
        </tr>
        <?
        arsort($write);

        $i = 0;
        foreach($write as $key => $value) {
            if($i++ >= 10) break;
            if($i <= 3) {
                $h_sql = "insert into hit_writer (bo_table, gubun, odr, mb_id) values ('".$bo_arr[$idx]."','wrt',".($i).",'".$value[1]['mb_id']."')";
                $result = sql_query($h_sql);
            }
            echo "<tr><td>".$value[1]['wr_name']."</td><td>".$value[0]."</td></tr>";
        }
        ?>
        </table>
    </td>
    <td style="width:70px;"></td>
    <td>
        <table border="0" cellpadding="2" cellspacing="0">
        <colgroup>
            <col style="width:200px;" />
            <col style="width:100px;" />
        </colgroup>
        <tr>
            <th>글쓴이</th><th>리플수</th>
        </tr>
        <?
        arsort($reply);

        $i = 0;
        foreach($reply as $key => $value) {
            if($i++ >= 10) break;
            echo "<tr><td>".$value[1]['wr_name']."</td><td>".$value[0]."</td></tr>";
        }
        ?>
        </table>
    </td>
</tr>
</table>
<hr class="hr2">
<? } ?>