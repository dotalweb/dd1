<?php
include_once("./_common.php");

if (!$_SESSION['ss_mb_id']) {
    header("location: ../index.php");
}

$g4[title] = "통계";
include_once("./_head.php");

@include ($g4['bbs_path']."/board_list.php");

$ym = date("Ym",strtotime("-1 month"));
?>
<style>
table, tr, th, td {font-size:9pt;}
hr {width:750px; height:1px; background:#b2b2b2; margin-bottom:10px; border:0;}
hr.hr2 {background:#848484; margin-bottom:20px;}
span.board_title {display:block; font-weight:bold; font-size:12pt; width:750px; background:#d2d2d2;}
</style>
<div style="height:20px;"></div>
<div>
<?php
if(intval(date("Y")) >= 2015) {
    if(date("Y") > 2015) echo ' <a href="half.php?y='.(intval(date("Y")) - 1).'&m=07">'.(intval(date("Y")) - 1).'년 하반기</a>';
    if(intval(date("m")) >= 7) echo ' <a href="half.php?y='.date("Y").'">'.date("Y").'년 상반기</a>';
}
?>
</div>
<?php
@include "stat_".$ym.".html";

include_once("./_tail.php");
?>