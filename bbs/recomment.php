<?php
include_once('./_common.php');
$save = array();
$cnt = 0;

$move_bo_table = "recomment";
$move_write_table = $g5['write_prefix'] . $move_bo_table;

$src_dir = "$g4[path]/data/file/$bo_table"; // 원본 디렉토리
$dst_dir = "$g4[path]/data/file/$move_bo_table"; // 복사본 디렉토리

$count_write = 0;
$count_comment = 0;

$next_wr_num = get_next_num($move_write_table);

//$sql2 = " select * from $write_table where wr_num = '$wr_num' order by wr_parent, wr_comment desc, wr_id ";
$sql2 = " select * from $write_table where wr_id = '$wr_id'";
$result2 = sql_fetch_array(sql_query($sql2));

if($result2['wr_good'] < 30 || ($result2['wr_good'] >= 30 && $result2['wr_10'] == ''))
{
    echo <<<HEREDOC
<script type="text/javascript">
opener.document.location.reload();
window.close();
</script>
HEREDOC;
    exit;
}

$sql2 = " select * from $write_table where wr_num = '{$result2['wr_num']}' order by wr_parent, wr_is_comment, wr_comment desc, wr_id ";
$result2 = sql_query($sql2);

while ($row2 = sql_fetch_array($result2))
{
    $nick = cut_str($member['mb_nick'], $config['cf_cut_name']);

    if($row2['ca_name'])
        $row2['wr_subject'] = "[".$row2['ca_name']."] ".$row2['wr_subject'];

    $sql = " insert into $move_write_table
                set wr_num            = '$next_wr_num',
                    wr_reply          = '{$row2['wr_reply']}',
                    wr_is_comment     = '{$row2['wr_is_comment']}',
                    wr_comment        = '{$row2['wr_comment']}',
                    wr_comment_reply  = '{$row2['wr_comment_reply']}',
                    ca_name           = '{$row2['ca_name']}',
                    wr_option         = '{$row2['wr_option']}',
                    wr_subject        = '{$row2['wr_subject']}',
                    wr_content        = '{$row2['wr_content']}',
                    wr_link1          = '{$row2['wr_link1']}',
                    wr_link2          = '{$row2['wr_link2']}',
                    wr_link1_hit      = '{$row2['wr_link1_hit']}',
                    wr_link2_hit      = '{$row2['wr_link2_hit']}',
                    wr_hit            = '{$row2['wr_hit']}',
                    wr_good           = '{$row2['wr_good']}',
                    wr_nogood         = '{$row2['wr_nogood']}',
                    mb_id             = '{$row2['mb_id']}',
                    wr_password       = '{$row2['wr_password']}',
                    wr_name           = '{$row2['wr_name']}',
                    wr_email          = '{$row2['wr_email']}',
                    wr_homepage       = '{$row2['wr_homepage']}',
                    wr_datetime       = '{$row2['wr_datetime']}',
                    wr_last           = '{$row2['wr_last']}',
                    wr_ip             = '{$row2['wr_ip']}',
                    wr_1              = '{$row2['wr_1']}',
                    wr_2              = '{$row2['wr_2']}',
                    wr_3              = '{$row2['wr_3']}',
                    wr_4              = '{$row2['wr_4']}',
                    wr_5              = '{$row2['wr_5']}',
                    wr_6              = '{$row2['wr_6']}',
                    wr_7              = '{$row2['wr_7']}',
                    wr_8              = '{$row2['wr_8']}',
                    wr_9              = '{$row2['wr_9']}',
                    wr_10             = '{$row2['wr_10']}' ";
    sql_query($sql);

    $insert_id = mysql_insert_id();

    // 코멘트가 아니라면
    if (!$row2['wr_is_comment'])
    {
        $save_parent = $insert_id;

        $sql3 = " select * from {$g5['board_file_table']} where bo_table = '{$bo_table} and wr_id = '{$row2['wr_id']}' order by bf_no ";
        $result3 = sql_query($sql3);
        for ($k=0; $row3 = sql_fetch_array($result3); $k++)
        {
            if ($row3['bf_file'])
            {
                // 원본파일을 복사하고 퍼미션을 변경
                @copy("$src_dir/$row3[bf_file]", "$dst_dir/$row3[bf_file]");
                @chmod("$dst_dir/$row3[bf_file]", 0606);
            }

            $sql = " insert into $g5[board_file_table]
                        set bo_table = '$move_bo_table',
                            wr_id = '$insert_id',
                            bf_no = '$row3[bf_no]',
                            bf_source = '$row3[bf_source]',
                            bf_file = '$row3[bf_file]',
                            bf_download = '$row3[bf_download]',
                            bf_content = '".addslashes($row3[bf_content])."',
                            bf_filesize = '$row3[bf_filesize]',
                            bf_width = '$row3[bf_width]',
                            bf_height = '$row3[bf_height]',
                            bf_type = '$row3[bf_type]',
                            bf_datetime = '$row3[bf_datetime]' ";
            sql_query($sql);
        }
		// 파일의 개수를 게시물에 업데이트 한다.
		$row3 = sql_fetch(" select count(*) as cnt from {$g5['board_file_table']} where bo_table = '{$bo_table}' and wr_id = '{$row2['wr_id']}' ");
		sql_query(" update {$move_bo_table} set wr_file = '{$row3['cnt']}' where wr_id = '{$insert_id}' ");		

        $count_write++;
    }
    else
    {
        $count_comment++;
    }

    sql_query(" update $move_write_table set wr_parent = '$save_parent' where wr_id = '$insert_id' ");

    $cnt++;
}

sql_query(" update $g5[board_table] set bo_count_write   = bo_count_write   + '$count_write'   where bo_table = '$move_bo_table' ");
sql_query(" update $g5[board_table] set bo_count_comment = bo_count_comment + '$count_comment' where bo_table = '$move_bo_table' ");
sql_query(" update $write_table set wr_10 = '' where wr_id = $wr_id' ");

echo <<<HEREDOC
<script type="text/javascript">
opener.document.location.reload();
window.close();
</script>
HEREDOC;
?>