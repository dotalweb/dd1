<?php
include_once("./_common.php");

if (!$member[mb_id])
    alert("회원만 이용하실 수 있습니다.");

$who_id = trim($_POST['who_id']);

$error_msg = "";
$sql = " select mb_id, mb_nick, mb_open, mb_leave_date, mb_intercept_date from $g4[member_table] where mb_id = '{$who_id}' ";
$row = sql_fetch($sql);

	////
	////__DEBUG
	////
	//if ( $_SERVER["REMOTE_ADDR"] == "115.139.199.83" ) {
	//	print "<b>".$_SERVER["SCRIPT_FILENAME"]."</b><BR>";
	//	print "<b>/*  ▶ */</b><BR>"; print "<font size=2>".$sql."</font>;"; print "<br><br>";
	//	exit;
	//}
	////
	////__DEBUG
	////

// 관리자가 아니면서
// 가입된 회원이 아니거나 정보공개를 하지 않았거나 탈퇴한 회원이거나 차단된 회원에게 쪽지를 보내는것은 에러
if ((!$row[mb_id] || !$row[mb_open] || $row[mb_leave_date] || $row[mb_intercept_date]) && !$is_admin) {
    $error_msg = $who_id;
}

if ($error_msg && !$is_admin)
    alert("회원아이디 \'".$error_msg."\' 은(는) 존재(또는 정보공개)하지 않는 회원아이디 이거나 탈퇴, 접근차단된 회원아이디 입니다.");

$tmp_row = sql_fetch(" select 1 from member_my_memo where mb_id = '$member[mb_id]' and who_id = '$who_id' ");
if(!$tmp_row) {
    $sql = " insert into member_my_memo
                    ( mb_id, who_id, reg_dt, memo )
             values ( '$member[mb_id]', '$who_id', '$g4[time_ymdhis]', '$_POST[my_memo]' ) ";
    sql_query($sql);
} else {
    $sql = " update member_my_memo set memo = '$_POST[my_memo]', reg_dt = '$g4[time_ymdhis]' where mb_id = '$member[mb_id]' and who_id = '$who_id' ";
    sql_query($sql);
}

alert("저장되었습니다.", "./profile.php?mb_id=$who_id");
?>