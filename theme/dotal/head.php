<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if (G5_IS_MOBILE) {
    include_once(G5_THEME_MOBILE_PATH.'/head.php');
    return;
}

include_once(G5_THEME_PATH.'/head.sub.php');
include_once(G5_LIB_PATH.'/latest.lib.php');
include_once(G5_LIB_PATH.'/outlogin.lib.php');
include_once(G5_LIB_PATH.'/poll.lib.php');
include_once(G5_LIB_PATH.'/visit.lib.php');
include_once(G5_LIB_PATH.'/connect.lib.php');
include_once(G5_LIB_PATH.'/popular.lib.php');

?>

<!-- 상단 시작 { -->
<div id="hd">
    <h1 id="hd_h1"><?php echo $g5['title'] ?></h1>
    <div id="skip_to_container"><a href="#container">본문 바로가기</a></div>

    <?php
    if(defined('_INDEX_')) { // index에서만 실행
        include G5_BBS_PATH.'/newwin.inc.php'; // 팝업레이어
    }
    ?>
    <div id="hd_wrapper">

        <div id="logo">
            <a href="<?php echo G5_URL ?>"><img src="<?php echo G5_IMG_URL ?>/dotal_logo.jpg" alt="<?php echo $config['cf_title']; ?>"></a>
        </div>
    
        <div class="hd_sch_wr">

        </div>
        <ul class="hd_login">        


        </ul>
    </div>
    
  
</div>
<!-- } 상단 끝 -->


<hr>

<!-- 콘텐츠 시작 { -->
<div id="wrapper">
    <div id="container_wr">
	<div id="container_nav">
		<div class="container_nav_sub_box">
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=notice">
				<div style="display:flex;align-items:center;<?php echo ($bo_table == 'notice' ? 'font-weight:800;background:#f1f3f6' : '')?>">
					공지사항 <img src="<?php echo G5_IMG_URL?>/notice_icon.png" style="width:18px;height:18px;margin-left:3px;">
				</div>
			</a>
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=free"><div style="<?php echo ($bo_table == 'free' ? 'font-weight:800;background:#f1f3f6' : '')?>">라이어게시판</div></a>
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=bizarre"><div style="<?php echo ($bo_table == 'bizarre' ? 'font-weight:800;background:#f1f3f6' : '')?>">엽기와의 만남</div></a>
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=masturbation">
				<div style="display:flex;align-items:center;<?php echo ($bo_table == 'masturbation' ? 'font-weight:800;background:#f1f3f6' : '')?>">
					정신적 딸딸이 <img src="<?php echo G5_IMG_URL?>/masturbation_icon.png" style="width:18px;height:18px;margin-left:3px;">
				</div>
			</a>
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=curious">
				<div style="<?php echo ($bo_table == 'curious' ? 'font-weight:800;background:#f1f3f6' : '')?>">
					호기심떡국 <img src="<?php echo G5_IMG_URL?>/curious_icon.png" style="width:18px;height:18px;margin-left:3px;">
				</div>
			</a>
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=election"><div style="<?php echo ($bo_table == 'election' ? 'font-weight:800;background:#f1f3f6' : '')?>">라이어의 선택</div></a>			
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=nulty">
				<div style="display:flex;align-items:center;<?php echo ($bo_table == 'nulty' ? 'font-weight:800;background:#f1f3f6' : '')?>">
					담배한개피 <img src="<?php echo G5_IMG_URL?>/nulty_icon.png" style="width:18px;height:18px;margin-left:3px;">
				</div>
			</a>
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=withgbs"><div style="<?php echo ($bo_table == 'withgbs' ? 'font-weight:800;background:#f1f3f6' : '')?>">구봉숙과함께</div></a>
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=news"><div style="<?php echo ($bo_table == 'news' ? 'font-weight:800;background:#f1f3f6' : '')?>">구봉숙 늬우스</div></a>
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=recomment">
				<div style="display:flex;align-items:center;<?php echo ($bo_table == 'recomment' ? 'font-weight:800;background:#f1f3f6' : '')?>">
					똥꼬 게시판 <img src="<?php echo G5_IMG_URL?>/recomment_icon.png" style="width:18px;height:18px;margin-left:3px;">
				</div>
			</a>
		</div>
		
		<div class="container_nav_sub_box">
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=share"><div style="<?php echo ($bo_table == 'share' ? 'font-weight:800;background:#f1f3f6' : '')?>">나눔 게시판</div></a>
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=niner"><div style="<?php echo ($bo_table == 'niner' ? 'font-weight:800;background:#f1f3f6' : '')?>">아홉수 게시판</div></a>
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=club_woman"><div style="<?php echo ($bo_table == 'club_woman' ? 'font-weight:800;background:#f1f3f6' : '')?>">놈들은 꺼져라</div></a>
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=sports">
				<div style="display:flex;align-items:center;<?php echo ($bo_table == 'sports' ? 'font-weight:800;background:#f1f3f6' : '')?>">
					스포츠 게시판 <img src="<?php echo G5_IMG_URL?>/sports_icon.png" style="width:18px;height:18px;margin-left:3px;">
				</div>
			</a>	
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=offline"><div style="<?php echo ($bo_table == 'offline' ? 'font-weight:800;background:#f1f3f6' : '')?>">모임방 게시판</div></a>			
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=test">
				<div style="display:flex;align-items:center;<?php echo ($bo_table == 'test' ? 'font-weight:800;background:#f1f3f6' : '')?>">
					전산실 <img src="<?php echo G5_IMG_URL?>/test_icon.png" style="width:18px;height:18px;margin-left:3px;">
				</div>
			</a>
			<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=settlement">
				<div style="display:flex;align-items:center;<?php echo ($bo_table == 'settlement' ? 'font-weight:800;background:#f1f3f6' : '')?>">
					사건해결의 방 <img src="<?php echo G5_IMG_URL?>/settle_icon.png" style="width:18px;height:18px;margin-left:3px;">
				</div>
			</a>			
			<a href="#" onclick="alert('투표페이지를 만들어주세요..')"><div style="<?php echo ($bo_table == 'news' ? 'font-weight:800;background:#f1f3f6' : '')?>">투표</div></a>			
		</div>	
		<div class="container_nav_sub_box">
			<a href="#" onclick="alert('도탈사전 만들어야..')">
				<div style="display:flex;align-items:center;<?php echo ($bo_table == 'dic' ? 'font-weight:800;background:#f1f3f6' : '')?>">
					도탈 사전 <img src="<?php echo G5_IMG_URL?>/dic_icon.png" style="width:18px;height:18px;margin-left:3px;">
				</div>
			</a>
			<a href="#" onclick="alert('생일자도 만들어야..')">
				<div style="display:flex;align-items:center;<?php echo ($bo_table == 'dic' ? 'font-weight:800;background:#f1f3f6' : '')?>">
					생일자 <img src="<?php echo G5_IMG_URL?>/birth_icon.png" style="width:18px;height:18px;margin-left:3px;">
				</div>			
			</a>
		</div>				
		<div class="container_nav_sub_box">
			<a href="#" onclick="alert('현황 만들어야 ..')"><div>현황</div></a>
			<a href="#" onclick="alert('CiO 만들어야..')"><div>CiO</div></a>
			<a href="#" onclick="alert('시간의방 만들어야...')"><div>시간의방</div>	</a>		
			<a href="#" onclick="alert('탈퇴신청 만들어야...')"><div>탈퇴신청</div>	</a>	
			<a href="#" onclick="alert('명예의전당포 만들어야..')"><div>명예의전당포</div>	</a>			
		</div>	
		<div><a href="<?php echo get_device_change_url(); ?>">모바일버전</a> | <a href="<?php echo G5_BBS_URL ?>/current_connect.php" class="visit">접속자<strong class="visit-num"><?php echo connect('theme/basic'); // 현재 접속자수, 테마의 스킨을 사용하려면 스킨을 theme/basic 과 같이 지정  ?></strong></a></div>
		
	</div>
    <div id="container">
        <?php if (!defined("_INDEX_") && !$bo_table) { ?><h2 id="container_title"><span title="<?php echo get_text($g5['title']); ?>"><?php echo get_head_title($g5['title']); ?></span></h2><?php } ?>

