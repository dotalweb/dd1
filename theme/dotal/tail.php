<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if (G5_IS_MOBILE) {
    include_once(G5_THEME_MOBILE_PATH.'/tail.php');
    return;
}
?>

    </div>
    <div id="aside">
        <?php echo outlogin('theme/basic'); // 외부 로그인, 테마의 스킨을 사용하려면 스킨을 theme/basic 과 같이 지정 ?>
        <?php //echo poll('theme/basic'); // 설문조사, 테마의 스킨을 사용하려면 스킨을 theme/basic 과 같이 지정 ?>
		<?php if ($member['mb_id']){?>
		<div id="aside_myboard">
			<h2>나의 게시글</h2>
			<ul>               
			<?php
			/*
			$r_list_talbe = 'notice,free,bizarre,masturbation,curious,election,nulty,withgbs,news,recomment';
			$r_cur_subject = 15; // 제목 글자수
			$r_limit = 5; // 출력 갯수
			$r_list_table_array = explode(',',$r_list_talbe);
			$r_where = '';
			$r_count_table = count($r_list_table_array);

			$r_i = 1;

			foreach ($r_list_table_array as $r_for_table) {       
				$r_for_table = trim($r_for_table);
				$r_select_table =$g5['write_prefix'].$r_for_table;
				$r_where .= "(
					select wr_subject, wr_comment, wr_id, wr_datetime, '$r_for_table' as bo_table
					FROM `$r_select_table` where mb_id = '{$member['mb_id']}' and wr_is_comment = '0' )";    
				
				if ($r_i != $r_count_table && 1 < $r_count_table) {
					$r_where .=" UNION " ;
				}         
				$r_i ++;        
			}
			$r_where .= 'order by wr_datetime desc ';
			$r_where .= "limit $r_limit";
			$r_query = sql_query($r_where);
			$r_href = '';
			while($row = sql_fetch_array($r_query)) {
				$r_href = G5_BBS_URL."/board.php?bo_table=".$row['bo_table']."&wr_id=".$row['wr_id'];
				echo "<li>";
				echo "<a href=".$r_href.">";
				echo cut_str($row['wr_subject'],$r_cur_subject);
				echo "</a>";
				echo "</li>";
			}
			*/
			?>
			</ul>
		</div>
		
		<div id="aside_myboard2">
			<h2>나의 댓글</h2>
			<ul>               
			<?php
			/*
			$r_i2 = 1;
			$r_where2 = "";
			foreach ($r_list_table_array as $r_for_table2) {       
				$r_for_table2 = trim($r_for_table2);
				$r_select_table =$g5['write_prefix'].$r_for_table2;
				$r_where2 .= "(
					select wr_content, wr_comment, wr_id, wr_datetime, '$r_for_table2' as bo_table
					FROM `$r_select_table` where mb_id = '{$member['mb_id']}' and wr_is_comment = '1' )";    
				
				if ($r_i2 != $r_count_table && 1 < $r_count_table) {
					$r_where2 .=" UNION " ;
				}         
				$r_i2 ++;        
			}
			$r_where2 .= 'order by wr_datetime desc ';
			$r_where2 .= "limit $r_limit";
			$r_query2 = sql_query($r_where2);
			$r_href2 = '';
			while($row = sql_fetch_array($r_query2)) {
				$r_href2 = G5_BBS_URL."/board.php?bo_table=".$row['bo_table']."&wr_id=".$row['wr_id'];
				echo "<li>";
				echo "<a href=".$r_href2.">";
				echo cut_str($row['wr_content'],$r_cur_subject);
				echo "</a>";
				echo "</li>";
			}
			*/
			?>
			</ul>
		</div>		
		
		
		<?php } ?>
    </div>
	
</div>
</div>
<!-- } 콘텐츠 끝 -->

<hr>

<!-- 하단 시작 { -->
<div id="ft">
    <div id="ft_wr">
        <div id="ft_link" class="ft_cnt">
            <b>2020 DOTAL</b> All rights reserved.
        </div>
		<div id="ft_copy">즐겁지 않으면 도탈이 아니다</div>
		<div></div>
	</div>  
 
    
    <button type="button" id="top_btn">
    	<i class="fa fa-arrow-up" aria-hidden="true"></i><span class="sound_only">상단으로</span>
    </button>
    <script>
    $(function() {
        $("#top_btn").on("click", function() {
            $("html, body").animate({scrollTop:0}, '500');
            return false;
        });
    });
    </script>
</div>

<?php
if(G5_DEVICE_BUTTON_DISPLAY && !G5_IS_MOBILE) { ?>
<?php
}

if ($config['cf_analytics']) {
    echo $config['cf_analytics'];
}
?>

<!-- } 하단 끝 -->

<script>
$(function() {
    // 폰트 리사이즈 쿠키있으면 실행
    font_resize("container", get_cookie("ck_font_resize_rmv_class"), get_cookie("ck_font_resize_add_class"));
});
</script>

<?php
include_once(G5_THEME_PATH."/tail.sub.php");
?>