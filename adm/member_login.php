<?php
$sub_menu = "200100";
include_once("./_common.php");

auth_check($auth[$sub_menu], "r");

$token = get_token();

$mb = get_member($mb_id);
if (!$mb['mb_id'])
    alert("존재하지 않는 회원자료입니다.");

if ($is_admin != 'super' && $mb['mb_level'] >= $member['mb_level'])
    alert("자신보다 권한이 높거나 같은 회원은 수정할 수 없습니다.");

$g5['title'] = "회원 로그인 기록";
include_once("./admin.head.php");

$sql = "select * from login_history where mb_id = '{$mb['mb_id']}' order by login_time desc";
$result = sql_query($sql);

?>

<div class="tbl_head01 tbl_wrap">
	<table width=100% align=center cellpadding=0 cellspacing=0>
	<colgroup width=20%>
	<colgroup width=40%>
	<colgroup width=40%>
	<tr>
		<td colspan=3 class=title align=left><img src='/adm/img/icon_title.gif'> <?php echo $g5['title']?>

		<?php
		echo G5_ADMIN_PATH;
		?>
		</td>
	</tr>
	<tr><td colspan=3 class=line1></td></tr>
	<tr class='bgcol1 bold col1 ht center'>
		<td><b>아이디</b></td>
		<td><b>로그인 시간</b></td>
		<td><b>IP</b></td>
	</tr>
	<tr><td colspan=3 class='line2'></td></tr>
	<?php
	for ($i=0; $row=sql_fetch_array($result); $i++) {
	?>
	<tr class='center'>
		<td><?=$row[mb_id]?></td>
		<td><?=$row[login_time]?></td>
		<td><?=$row[login_ip]?></td>
	</tr>
	<?php
		}
	?>
	<tr><td colspan=3 class=line2></td></tr>
	</table>
</div>

<p align=center>
    <input type=button class=btn1 onClick="javascript:window.close();" value='  닫    기  '>
</p>

<?
include_once("./admin.tail.php");
?>
