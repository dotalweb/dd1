<?
include_once("./_common.php");
include_once("$g4[path]/lib/mailer.lib.php");

/*
// 081022 : CSRF 에서 토큰 비교는 의미 없음
// 세션에 저장된 토큰과 폼값으로 넘어온 토큰을 비교하여 틀리면 에러
if ($_POST["token"] && get_session("ss_token") == $_POST["token"])
{
    // 이전 폼 전송 바로전에 만들어진 쿠키가 없다면 에러
    //if (!get_cookie($_POST["token"])) alert_close("쿠키 에러");

    // 맞으면 세션과 쿠키를 지워 다시 입력폼을 통해서 들어오도록 한다.
    set_session("ss_token", "");
    set_cookie($_POST["token"], 0, 0);
}
else
{
    alert_close("토큰 에러");
    exit;
}
*/

// 리퍼러 체크
//referer_check();

if ($w == "u" && $is_admin == "super") {
    if (file_exists("$g4[path]/DEMO"))
        alert("데모 화면에서는 하실(보실) 수 없는 작업입니다.");
}

// 자동등록방지 검사
//include_once ("./norobot_check.inc.php");

$mb_id = trim(strip_tags(mysql_real_escape_string($_POST[mb_id])));

if (!$mb_id) alert('회원아이디가 넘어오지 않았습니다.');

$mb = get_member($mb_id);

// 관리자님 회원정보
$admin = get_admin('super');

// 회원님께 메일 발송
if ($mb[mb_email])
{
    $subject = "메일 재인증";

    $sql = " update $g4[member_table]
                set mb_email_certify = ''
              where mb_id = '$mb[mb_id]' ";
    sql_query($sql);

    //$mb_md5 = md5($mb[mb_id].$mb[mb_email].$g4[time_ymdhis]);
    $mb_md5 = md5($mb[mb_id].$mb[mb_email].$mb[mb_datetime]);
    $certify_href = "$g4[url]/$g4[bbs]/email_certify.php?mb_id=$mb_id&mb_md5=$mb_md5";

    ob_start();
    include_once ("../bbs/register_form_update_mail3.php");
    $content = ob_get_contents();
    ob_end_clean();

    mailer($admin[mb_nick], $admin[mb_email], $mb[mb_email], $subject, $content, 1);
}


// 사용자 코드 실행
@include_once ("$g4[path]/skin/member/$config[cf_member_skin]/register_update.skin.php");

if ($msg)
    echo "<script type='text/javascript'>alert('{$msg}');</script>";

echo "
<html><title>메일 재인증</title><meta http-equiv='Content-Type' content='text/html; charset=$g4[charset]'></html><body>
<script type='text/javascript'>
alert('재인증 메일(".$mb[mb_email].")이 전송되었습니다.');
window.close();
</script>
</body>
</html>";
?>
