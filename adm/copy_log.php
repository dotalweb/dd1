<?php
$sub_menu = "200840";
include_once("./_common.php");

auth_check($auth[$sub_menu], "r");

$search_sort = $_GET['search_sort'];

$g5['title'] = "불펌체크";
include_once("./admin.head.php");

include_once("$g4[path]/lib/visit.lib.php");

$qstr = "search_word=$search_word&search_sort=$search_sort"; //페이징 처리관련 변수

$colspan = 4;

$listall = "<a href='{$_SERVER['PHP_SELF']}' class=tt>처음</a>"; //페이지 처음으로 (초기화용도)
?>

<!-- 달력 datepicker 시작 -->
<link type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.4/themes/base/jquery-ui.css" rel="stylesheet" />
<style>
.ui-datepicker { font:12px dotum }
.ui-datepicker select.ui-datepicker-month,
.ui-datepicker select.ui-datepicker-year { width: 70px;}
.ui-datepicker-trigger { margin:0 0 -5px 2px }
.search_sort {width:100px;vertical-align:middle}
.ed {vertical-align:middle}
</style>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.4/jquery-ui.min.js"></script>
<script type="text/javascript">
jQuery(function($){
    $.datepicker.regional["ko"] = {
        closeText: "닫기",
        prevText: "이전달",
        nextText: "다음달",
        currentText: "오늘",
        monthNames: ["1월(JAN)","2월(FEB)","3월(MAR)","4월(APR)","5월(MAY)","6월(JUN)", "7월(JUL)","8월(AUG)","9월(SEP)","10월(OCT)","11월(NOV)","12월(DEC)"],
        monthNamesShort: ["1월","2월","3월","4월","5월","6월", "7월","8월","9월","10월","11월","12월"],
        dayNames: ["일","월","화","수","목","금","토"],
        dayNamesShort: ["일","월","화","수","목","금","토"],
        dayNamesMin: ["일","월","화","수","목","금","토"],
        weekHeader: "Wk",
        dateFormat: "yymmdd",
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: true,
        yearSuffix: ""
    };
    $.datepicker.setDefaults($.datepicker.regional["ko"]);
});
</script>
<!-- 달력 datepicker 끝 -->

<table width="100%" cellpadding="3" cellspacing="1">
<form name="fvisit" method="get">
<tr>
    <td class="sch_wrp">
        <?=$listall?>
        <label for="sch_sort">검색분류</label>
        <select name="search_sort" id="sch_sort" class="search_sort">
            <?php
            if($search_sort=='mb_id'){ //select 안의 옵셥값이 mb_id면
                echo '<option value="mb_id" selected="selected">회원ID</option>'; //selected 추가
            }else{
                echo '<option value="mb_id">회원ID</option>';
            }
            if($search_sort=='wr_id'){ //select 안의 옵셥값이 wr_id
                echo '<option value="wr_id" selected="selected">게시물번호</option>'; //selected 추가
            }else{
                echo '<option value="wr_id">게시물번호</option>';
            }
            if($search_sort=='cp_datetime'){ //select 안의 옵셥값이 cp_datetime면
                echo '<option value="cp_datetime" selected="selected">날짜</option>'; //selected 추가
            }else{
                echo '<option value="cp_datetime">날짜</option>';
            }
            ?>
        </select>
        <input type="text" name="search_word" size="20" value="<?=$search_word?>" id="sch_word" class="ed">
        <input type="image" src="<?=$g4['admin_path']?>/img/btn_search.gif" alt="검색" align="absmiddle" onclick="fvisit_submit('copy_log.php');">
    </td>
</tr>
</form>
</table>

<table width="100%" cellpadding="0" cellspacing="1" border="0">
<form name="fdel" method="post">
<colgroup width="150">
<colgroup width="">
<colgroup width="100">
<colgroup width="100">
<tr><td colspan="<?=$colspan?>" class="line1"></td></tr>
<tr class="bgcol1 bold col1 ht center">
    <td>게시판</td>
    <td>게시물</td>
    <td>ID</td>
    <td>일시</td>
</tr>
<tr><td colspan="<?=$colspan?>" class="line2"></td></tr>
<?php
$sql_common = " from content_copy ";
if ($search_sort) {
    if($search_sort=='cp_datetime'){
        $sql_search = " where $search_sort like '$search_word%' ";
    }else if($search_sort=='wr_id'){
        $sql_search = " where $search_sort = '$search_word%' ";
    }else{
        $sql_search = " where $search_sort like '%$search_word%' ";
    }
}
$sql = " select count(*) as cnt
         $sql_common
         $sql_search ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page == "") $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select *
          $sql_common
          $sql_search
          order by cp_datetime desc
          limit $from_record, $rows ";
$result = sql_query($sql);

for ($i=0; $row=sql_fetch_array($result); $i++) {
    if ($is_admin == 'super')
        $ip = $row['ip'];
    else
        $ip = preg_replace("/([0-9]+).([0-9]+).([0-9]+).([0-9]+)/", "\\1.♡.\\3.\\4", $row['ip']);

    $list = ($i%2);
	$mb = get_member($row['mb_id']);
    echo "
    <tr class='list$list col1 ht center'>
        <td align=left><nobr style='display:block; overflow:hidden;'>$row[bo_table]</nobr></td>
        <td align=left><nobr style='display:block; overflow:hidden;'><a href='/bbs/board.php?bo_table=$row[bo_table]&wr_id=$row[wr_id]' target='_blank'>/bbs/board.php?bo_table=$row[bo_table]&wr_id=$row[wr_id]</a></nobr></td>
        <td>".get_sideview($mb['mb_id'],$mb['mb_nick'])."</td>
        <td><a href='{$_SERVER['PHP_SELF']}?search_sort=search_sort&amp;search_word=".substr($row['cp_datetime'],0,10)."'>$row[cp_datetime]</a></td>
    </tr>";
}

if ($i == 0)
    echo "<tr><td colspan='$colspan' height=100 align=center>자료가 없습니다.</td></tr>";

echo "<tr><td colspan='$colspan' class='line2'></td></tr>";
echo "</form></table>";

$page = get_paging($config['cf_write_pages'], $page, $total_page, "$_SERVER[PHP_SELF]?$qstr&page=");
if ($page) {
    //echo "<table width=100% cellpadding=3 cellspacing=1><tr><td align=left><input type='button' onclick='del();' value='삭제' /></td><td align=right>$page</td></tr></table>";
    echo "<table width=100% cellpadding=3 cellspacing=1><tr><td align=right>$page</td></tr></table>";
}
?>

<script type='text/javascript'>
function del() {
    var f = document.fdel;
    var chk_count = 0;
    for (var i=0; i<f.length; i++) {
        if (f.elements[i].name == "chk_idx[]" && f.elements[i].checked)
            chk_count++;
    }
    if(chk_count) {
        if(confirm("선택한 데이터를 삭제하시겠습니까?")) {
            f.submit();
        }
    }
}

$(function(){
    $("#sch_sort").change(function(){ // select #sch_sort의 옵션이 바뀔때
        if($(this).val()=="cp_datetime"){ // 해당 value 값이 cp_datetime이면
            $("#sch_word").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", maxDate: "+0d" }); // datepicker 실행
        }else{ // 아니라면
            $("#sch_word").datepicker("destroy"); // datepicker 미실행
        }
    });
    if($("#sch_sort option:selected").val()=="cp_datetime"){ // select #sch_sort 의 옵션중 selected 된것의 값이 cp_datetime라면
        $("#sch_word").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", maxDate: "+0d" }); // datepicker 실행
    }
});

function fvisit_submit(act)
{
    var f = document.fvisit;
    f.action = act;
    f.submit();
}
</script>

<?php
include_once("./admin.tail.php");
?>
